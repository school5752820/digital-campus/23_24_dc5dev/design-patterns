class Singleton {
  private static instance: Singleton;

  private constructor() {}

  public static getInstance(): Singleton {
    if (!Singleton.instance) {
      console.log("Create instance...");
      Singleton.instance = new Singleton();
    }

    return Singleton.instance;
  }

  public connectToDatabase() {
    console.log("Connecting to DB...");
  }
}

const instance1 = Singleton.getInstance();
const instance2 = Singleton.getInstance();

console.log({
  instance1: instance1.connectToDatabase(),
  instance2: instance2.connectToDatabase(),
});
