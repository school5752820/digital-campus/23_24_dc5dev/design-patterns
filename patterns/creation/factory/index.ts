abstract class DBConnectionFactory {
  public abstract createDBConnection(): DBConnection;
}

abstract class DBConnection {
  provider: string;

  public connect() {
    console.log("Connecté au provider : " + this.provider);
  }
}

class MySQLConnectionFactory extends DBConnectionFactory {
  public createDBConnection() {
    return new MySQLConnection();
  }
}

class PostgresConnectionFactory extends DBConnectionFactory {
  public createDBConnection() {
    return new PostgresConnection();
  }
}

class MySQLConnection extends DBConnection {
  constructor() {
    super();
    this.provider = "MySQL";
  }
}

class PostgresConnection extends DBConnection {
  constructor() {
    super();
    this.provider = "Postgres";
  }
}

function create(dbFactory: DBConnectionFactory) {
  const dbConnection = dbFactory.createDBConnection();
  dbConnection.connect();
}

create(new PostgresConnectionFactory());
