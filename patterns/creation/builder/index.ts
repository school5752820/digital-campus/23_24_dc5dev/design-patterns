class Product {
  public name: string = "";
  public description: string = "";
  public thumbnail: string = "";
  public price: number = 0;
  public stock: number = 0;
  public category: string = "";

  public setName(name: string) {
    this.name = name;
  }

  public setDescription(desc: string) {
    this.description = desc;
  }

  public setThumbnail(thumbnail: string) {
    this.thumbnail = thumbnail;
  }

  public setPrice(price: number) {
    this.price = price;
  }

  public setStock(stock: number) {
    this.stock = stock;
  }

  public setCategory(category: string) {
    this.category = category;
  }
}

interface Builder<T, U> {
  reset(): T;
  get(): U;
}

class ProductBuilder implements Builder<ProductBuilder, Product> {
  #product: Product;

  constructor() {
    this.reset();
  }

  public reset() {
    this.#product = new Product();
    return this;
  }

  get() {
    const product = this.#product;

    return product;
  }

  public setName(name: string) {
    this.#product.name = name;

    return this;
  }

  public setDescription(desc: string) {
    this.#product.description = desc;

    return this;
  }

  public setThumbnail(thumbnail: string) {
    this.#product.thumbnail = thumbnail;

    return this;
  }

  public setPrice(price: number) {
    this.#product.price = price;

    return this;
  }

  public setStock(stock: number) {
    this.#product.stock = stock;

    return this;
  }

  public setCategory(category: string) {
    this.#product.category = category;

    return this;
  }
}

const productBuilder = new ProductBuilder();
const product = productBuilder
  .setName("")
  .setPrice(10)
  .setCategory("category")
  .get();

console.log({ product });
