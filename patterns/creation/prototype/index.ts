interface Cloneable<T> {
  clone(): T;
}

class ComponentPrototype implements Cloneable<ComponentPrototype> {
  clone(): ComponentPrototype {
    return { ...this };
  }
}

class Document extends ComponentPrototype {
  components: ComponentPrototype[] = [];

  clone() {
    const clonedDocument = new Document();
    clonedDocument.components = this.components.map((c) => c.clone());
    return clonedDocument;
  }

  add(component: ComponentPrototype) {
    this.components.push(component);
  }
}

class Title extends ComponentPrototype {
  constructor(public content: string) {
    super();
  }

  updateText(content: string) {
    this.content = content;
  }
}

class Image extends ComponentPrototype {
  constructor(
    public url: URL,
    public legend: string,
  ) {
    super();
  }

  updateImage(url: URL) {
    this.url = url;
  }

  updateLegend(legend: string) {
    this.legend = legend;
  }
}

const document = new Document();
const title = new Title("Mon joli titre");
const image = new Image(
  new URL("https://source.unsplash.com/random"),
  "Ma légende",
);

document.add(title);
document.add(image);

const clonedDocument = document.clone();

title.updateText("Mon titre à jour");
image.updateLegend("Mon texte alternatif");

console.log({
  document: document.components.map((c) => JSON.stringify(c)),
  clonedDocument: clonedDocument.components.map((c) => JSON.stringify(c)),
});
