interface AttackBehavior {
  attack(character: Character): void;
}

abstract class Character {
  private attackBehavior: AttackBehavior;
  private health: number = 100;
  public name: string;
  private level: number = 0;

  constructor(attackBehavior: AttackBehavior, name: string) {
    this.attackBehavior = attackBehavior;
    this.name = name;
  }

  setAttackBehavior(attackBehavior: AttackBehavior) {
    this.attackBehavior = attackBehavior;
  }

  setHealth(cb: (health: number) => number) {
    this.health = cb(this.health);
  }

  getHealth() {
    return this.health;
  }

  levelUp() {
    this.level++;
  }

  levelDown() {
    this.level--;
  }

  attack(character: Character) {
    this.attackBehavior.attack(character);
    console.log(
      `Touché ! ${character.name} est blessé. Santé restante: ${character.getHealth()}`,
    );
  }
}

class SwordAttack implements AttackBehavior {
  attack(character: Character) {
    character.setHealth((health) => health - 10);
  }
}

class BowAttack implements AttackBehavior {
  attack(character: Character) {
    character.setHealth((health) => health - 5);
  }
}

class KnifeAttack implements AttackBehavior {
  attack(character: Character) {
    character.setHealth((health) => health - 15);
  }
}

class BombAttack implements AttackBehavior {
  attack(character: Character) {
    character.setHealth((health) => health - 50);
  }
}

class Troll extends Character {
  constructor(name: string) {
    super(new KnifeAttack(), name);
  }
}

class Elf extends Character {
  constructor(name: string) {
    super(new BowAttack(), name);
  }
}

class Knight extends Character {
  constructor(name: string) {
    super(new SwordAttack(), name);
  }
}

const troll = new Troll("Arthur");
const elf = new Elf("Legolas");
const knight = new Knight("Galahad");

elf.attack(knight);

elf.setAttackBehavior(new BombAttack());

elf.attack(knight);
