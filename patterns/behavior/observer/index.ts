import { EventEmitter } from "events";

class VolumeControllerSubject extends EventEmitter {
  #volume = 50;

  get volume() {
    return this.#volume;
  }

  constructor() {
    super();
    this.on("newListener", (eventName) => {
      console.log(`New listener: ${eventName}`);
    });
    this.on("removeListener", (eventName) => {
      console.log(`Removed listener: ${eventName}`);
    });
  }

  volumeUp() {
    this.#volume += 10;
    this.emit("volumeUp", this.#volume);
  }

  volumeDown() {
    this.#volume -= 10;
    this.emit("volumeDown", this.#volume);
  }
}

class LoggingObserver {
  constructor(private volumeController: VolumeControllerSubject) {
    this.volumeController.on("volumeUp", this.volumeUpObserver);
    this.volumeController.on("volumeDown", this.volumeDownObserver);
  }

  private volumeUpObserver(volume: number) {
    console.log(`Volume up: ${volume}`);
  }

  private volumeDownObserver(volume: number) {
    console.log(`Volume down: ${volume}`);
  }

  unsubscribe() {
    this.volumeController.removeListener("volumeUp", this.volumeUpObserver);
  }
}

const volumeController = new VolumeControllerSubject();
const loggingObserver = new LoggingObserver(volumeController);

volumeController.volumeUp();
volumeController.volumeUp();
volumeController.volumeDown();
volumeController.volumeDown();

loggingObserver.unsubscribe();

volumeController.volumeUp();
volumeController.volumeDown();
volumeController.volumeUp();
volumeController.volumeDown();
