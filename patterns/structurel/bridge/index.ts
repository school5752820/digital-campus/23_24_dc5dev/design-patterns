abstract class ItemViewAbstraction {
  constructor(protected contentType: ContentTypeImplementation) {}

  abstract render(): string;
}

interface ContentTypeImplementation {
  renderTitle(): string;
  renderContent(): string;
}

class SemanticItemView extends ItemViewAbstraction {
  render() {
    return `<div class="hero">
    <h2>${this.contentType.renderTitle()}</h2>
    <p>${this.contentType.renderContent()}</p>
</div>`;
  }
}

class LolItemView extends ItemViewAbstraction {
  render() {
    return `<span class="feature">
    <button>${this.contentType.renderTitle()}</button>
    <head>${this.contentType.renderContent()}</head>
</div>
`;
  }
}

class HeroContentType implements ContentTypeImplementation {
  constructor(
    protected title: string,
    protected content: string,
    protected thumbnail: string,
  ) {}

  renderTitle() {
    return "Hero Title";
  }

  renderContent() {
    return "Hero Content";
  }

  renderThumbnail() {
    return "Hero Thumbnail";
  }
}

class FeatureContentType implements ContentTypeImplementation {
  constructor(
    protected title: string,
    protected content: string,
    protected price: number,
  ) {}

  renderTitle() {
    return "Feature Title";
  }

  renderContent() {
    return "Feature Content";
  }

  renderPrice() {
    return "Feature Price";
  }
}

const blocks = [
  new HeroContentType(
    "Hero Title",
    "Hero Content",
    "https://example.com/hero.jpg",
  ),
  new FeatureContentType("Feature Title", "Feature Content", 100),
  new FeatureContentType("Feature Title", "Feature Content", 200),
  new FeatureContentType("Feature Title", "Feature Content", 300),
];

/*const semanticViews = blocks.map((block) => new SemanticItemView(block));

console.log(
  "Semantic Views: ",
  semanticViews.map((view) => view.render()).join("\n"),
);
*/

const lolViews = blocks.map((block) => new LolItemView(block));
console.log("Lol Views: ", lolViews.map((view) => view.render()).join("\n"));
