interface WeatherService {
  request(): Promise<WeatherForecast>;
}

type WeatherTypology = "sun" | "rain" | "cloud";

interface WeatherForecast {
  temperature: number;
  weather: WeatherTypology;
}

class WeatherServiceSDK implements WeatherService {
  async request(): Promise<WeatherForecast> {
    console.log("Calling SDK...");
    const weather = {
      temperature: Math.random() * 40,
      weather: ["sun", "rain", "cloud"][
        Math.floor(Math.random() * 3)
      ] as WeatherTypology,
    };

    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(weather);
      }, 1000);
    });
  }
}

class ProxyWeatherService implements WeatherService {
  private cachedResponse: WeatherForecast;
  private cachedDate: Date;
  private expirationTime = 1000 * 60 * 5; // 5 minutes

  constructor(protected weatherService: WeatherService) {}

  async request(): Promise<WeatherForecast> {
    console.log("Requesting weather on " + new Date().toISOString());
    const initialTime = Date.now();

    if (this.isCacheExpired()) {
      console.log("Invalid cache, call SDK...");
      this.setCache(await this.weatherService.request());
    }

    const requestTime = Date.now() - initialTime;
    console.log("Request time: " + requestTime + "ms");

    return this.cachedResponse;
  }

  private isCacheExpired(): boolean {
    return this.cachedResponse && this.cachedDate
      ? Date.now() > this.cachedDate.getTime() + this.expirationTime
      : true;
  }

  private setCache(response: WeatherForecast): void {
    this.cachedResponse = response;
    this.cachedDate = new Date();
  }
}

async function test(weatherService: WeatherService) {
  for (let i = 0; i < 5; i++) {
    const forecast = await weatherService.request();
    console.log(
      "La météo est de",
      forecast.weather,
      "avec une température de",
      forecast.temperature,
    );
  }
}

const sdk = new WeatherServiceSDK();
await test(new ProxyWeatherService(sdk));
