interface SchoolFeeCalculator {
  computePriceInEuros(year: number, isAlternanceship: boolean): number;
}

class DCSchoolFeeCalculator implements SchoolFeeCalculator {
  computePriceInEuros(year: number, isAlternanceship: boolean): number {
    const yearsPrice = 5_000 * year;

    if (isAlternanceship) {
      return yearsPrice + 20_000;
    }

    return yearsPrice;
  }
}

class CambridgeSchoolFeeCalculator {
  computePriceInDollars(year: number, isAlternanceship: boolean): number {
    const yearsPrice = 80_000 * year;

    if (isAlternanceship) {
      return yearsPrice + 500_000;
    }

    return yearsPrice;
  }
}

class CambridgeSchoolFeeCalculatorAdapter implements SchoolFeeCalculator {
  constructor(private adaptee: CambridgeSchoolFeeCalculator) {}

  computePriceInEuros(year: number, isAlternanceship: boolean): number {
    return this.adaptee.computePriceInDollars(year, isAlternanceship) * 1.4;
  }
}

function compute(calculator: SchoolFeeCalculator) {
  const price = calculator.computePriceInEuros(3, true);
  console.log({ price });
}

compute(new DCSchoolFeeCalculator());
compute(
  new CambridgeSchoolFeeCalculatorAdapter(new CambridgeSchoolFeeCalculator()),
);
