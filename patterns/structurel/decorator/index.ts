interface Request {
  url: string;
  method: string;
  data?: any;
}

interface Response {
  status: number;
  data: any;
}

interface Controller {
  handle(request: Request): Promise<Response>;
}

class ListProductsController implements Controller {
  async handle(request: Request): Promise<Response> {
    const products = [
      { id: 1, name: "Product 1" },
      { id: 2, name: "Product 2" },
      { id: 3, name: "Product 3" },
      { id: 4, name: "Product 4" },
    ];

    const response: Response = {
      status: 200,
      data: products,
    };

    if (request.method !== "GET") {
      response.status = 405;
      response.data = { message: "Method not allowed" };
    }

    return new Promise((resolve) => {
      console.log({ response });
      setTimeout(() => {
        resolve(response);
      }, 200);
    });
  }
}

class Decorator implements Controller {
  constructor(protected controller: Controller) {}

  async handle(request: Request): Promise<Response> {
    return this.controller.handle(request);
  }
}

class TelemetryDecorator extends Decorator {
  public async handle(request: Request): Promise<Response> {
    const startTime = new Date().getTime();

    const result = await super.handle(request);

    const endTime = new Date().getTime();
    const duration = endTime - startTime;

    console.log(
      `Request to ${request.url} with method ${request.method} took ${duration}ms`,
    );

    return result;
  }
}

const productsController = new TelemetryDecorator(new ListProductsController());
await productsController.handle({ url: "/products", method: "GET" });
